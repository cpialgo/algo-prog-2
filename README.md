# ALGO PROG 2

## Objectifs du cours

Savoir résoudre un problème donné en concevant un programme informatique à partir de techniques algorithmiques standards.

#### Algorithme 

- Algorithmique 
	‐ structures de données (listes chaînées, piles, files, arbres, graphes), 
	‐ tris, 
	‐ méthode « diviser pour régner », 
	‐ analyse de complexité. 

#### Programmation

- Présentation générale du langage C : 
	‐ instructions et types de base, 
	‐ fonctions, ‐pointeurs, 
	‐ fichiers. 

## Déroulement du cours
| Date |Type |Intitulé | Ressources | Autres |
|--|--|--|--|--|
| 04 Fev (10h00-12h00) | TD | [Introduction](https://gitlab.com/cpialgo/algo-prog-2/blob/master/d/01.md) |  |  |
| 04 Fev (13h30-16h30) | CM |  |  |  |
| 07 Fev (15h30-17h30) | TD |  |  |  |
| 12 Fev (10h00-12h00) | TD |  |  |  |
| 13 Fev (10h00-12h00) | TD |  |  |  |
| 26 Fev (13h30-16h30) | TD |  |  |  |
| 27 Fev (13h30-16h30) | TD |  |  |  |
| 05 Mar (10h00-12h00) | TD |  |  |  |

	

<!--stackedit_data:
eyJoaXN0b3J5IjpbODU4MTU1MDY0LDE4MTQ3NjI1NDUsMTQ4MT
k3MzMzXX0=
-->